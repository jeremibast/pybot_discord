# bot.py
import os
import random
import discord
from discord import channel
from discord import message
from discord.ext import commands
from os.path import join, dirname
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


TOKEN = os.environ.get('DISCORD_TOKEN')
GUILD = os.environ.get('DISCORD_GUILD')

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user.name} has connected to Discord!')

@client.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to my Discord server!'
    )

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    army_quotes = [
        'Select your character : ninja :ninja: elf :woman_elf: supervillain :woman_supervillain: mage :man_mage:',
         'Select your character : detective :man_detective: artist :woman_artist: farmer :man_farmer: mermaid :mermaid:'
    ]

    message_content = message.content.lower()
    if 'select' in message_content:
        response = random.choice(army_quotes)
        await message.channel.send(response)

    if 'elf' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :woman_elf:,votre arme est :bow_and_arrow: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :woman_elf:,votre arme est :eagle: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "GGame Over Try again!"    
        await message.channel.send(results)      

    if 'supervillain' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :woman_supervillain:,votre arme est :mechanical_arm: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :woman_supervillain:,votre arme est :eyeglasses: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results)

    if 'mage' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :man_mage:,votre arme est :fire: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :man_mage:,votre arme est :magic_wand: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results)

    if 'detective' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :man_detective:,votre arme est :camera_with_flash: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :man_detective:,votre arme est :syringe: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results)

    if 'artist' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :woman_artist:,votre arme est :mouse_trap: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :woman_artist:,votre arme est :paintbrush: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results)

    if 'farmer' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :man_farmer:,votre arme est :axe: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :man_farmer:,votre arme est :tractor: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results) 

    if 'mermaid' in message_content:
        rand_life = random.randint(0,5)
        if rand_life >1 and rand_life < 3 :
            results = "Vous avez choisit :mermaid:,votre arme est :shark: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        elif rand_life > 3:
            results = "Vous avez choisit :mermaid:,votre arme est  :fishing_pole_and_fish: et votre nombre de point de vie est "+str(rand_life)+" Be Brave !"
        else :
            results = "Game Over Try again!"    
        await message.channel.send(results)


    if 'flip' in message_content:
        rand_int = random.randint(0,1)
        if rand_int == 0:
            results = "heads"
        else:
            results = "tails"
        await message.channel.send(results)        


# @client.event
# async def on_reaction_add(reaction, user):
#     if reaction.emoji == ':woman_elf:':
#         channel = reaction.message.channel
#         await message.channel.send(channel, '{} a choisit {} comme personnage {}'.format(user.name, reaction.emoji, reaction.message.content))

# @client.event
# async def on_reaction_remove(reaction, user):
#         channel = reaction.message.channel
#         await client.send_message(channel, '{} a choisit {} comme personnage {}'.format(user.name, reaction.emoji, reaction.message.content))

# @client.command(pass_context=True)
# async def clear (ctx, amout=100):
#     channel = ctx.message.channel
#     messages = []
#     async for message in client.logs_from(channel, limit=int(amout)):
#         messages.append(message)
#     await client.delete_messages(messages)

client.run(TOKEN)

# @bot.command(name='roll_dice', help='Simulates rolling dice.')
# async def roll(ctx, number_of_dice: int, number_of_sides: int):
#     dice = [
#         str(random.choice(range(1, number_of_sides + 1)))
#         for _ in range(number_of_dice)
#     ]
#     await ctx.send(', '.join(dice))
# bot.run(TOKEN)

# @client.event
# async def on_message(message):
#     if message.author == client.user:
#         return

#     brooklyn_99_quotes = [
#         'I\'m the human form of the 💯 emoji.',
#         'Bingpot!',
#         (
#             'Cool. Cool cool cool cool cool cool cool, '
#             'no doubt no doubt no doubt no doubt.'
#         ),
#     ]

#     if message.content == '99!':
#         response = random.choice(brooklyn_99_quotes)
#         await message.channel.send(response)

  # if message.content == 'army':
    #     response = random.choice(army_quotes)
    #     await message.channel.send(response)